import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;

public class ExpensesReportProcessorTest
{

  @Test
  public void outputSingleLine() throws Exception
  {

    String expected = "Date\tCategory\tRequested Amount\tStatus\tAllowedAmount\n" +
      "10 March\tfood\t11.5\tOK\t11.5";

    String jsonPath = "src/main/resources/request.json";

    String result = new ExpensesReportProcessor(new ExpensesRequestLoader(), new ExpensesRequestValidator(
      new ExpensesValidationPolicy(
        new ExpenseWithDocumentValidationRule(),
        new AmountValidationRule(),
        new FoodAmountValidationRule())),
      new ReportGenerator()).process(jsonPath);

    Assert.assertThat(result, Is.is(expected));
  }
}
import java.util.ArrayList;
import java.util.List;

public class ExpensesRequest
{

  private List<Expense> expenses = new ArrayList<Expense>();
  private String month;
  private int year;
  private String status;

  public List<Expense> getExpenses()
  {
    return expenses;
  }

  public void setExpenses(final List<Expense> expenses)
  {
    this.expenses = expenses;
  }

  public String getMonth()
  {
    return month;
  }

  public void setMonth(final String month)
  {
    this.month = month;
  }

  public int getYear()
  {
    return year;
  }

  public void setYear(final int year)
  {
    this.year = year;
  }

  public String getStatus()
  {
    return status;
  }

  public void setStatus(final String status)
  {
    this.status = status;
  }
}

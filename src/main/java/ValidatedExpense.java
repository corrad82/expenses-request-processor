import java.math.BigDecimal;
import java.util.Date;

 class ValidatedExpense
{
  private final Date day;
  private final String category;
  private final BigDecimal amount;
  private Status status;
  private BigDecimal allowedAmount;

  private ValidatedExpense(final Date day, final String category, final BigDecimal amount)
  {

    this.day = day;
    this.category = category;
    this.amount = amount;
    this.allowedAmount = amount;
    this.status = Status.OK;
  }

   static ValidatedExpense from(final Expense expense)
  {
    ValidatedExpense validatedExpense = new ValidatedExpense(expense.getDay(),
      expense.getCategory(), expense.getAmount());
    return validatedExpense;
  }

  public void setStatus(final Status status)
  {
    this.status = status;
  }

  public Status getStatus()
  {
    return status;
  }

  public void setAllowedAmount(final BigDecimal allowedAmount)
  {
    this.allowedAmount = allowedAmount;
  }

  public BigDecimal getAllowedAmount()
  {
    return allowedAmount;
  }

  public Date getDay()
  {
    return day;
  }

  public String getCategory()
  {
    return category;
  }

  public BigDecimal getAmount()
  {
    return amount;
  }

  enum Status
  {
    NODOC, PARTIAL, OK
  }
}

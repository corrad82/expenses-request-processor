import org.apache.commons.lang3.time.DateFormatUtils;

import java.util.List;
import java.util.stream.Collectors;

public class ReportGenerator
{
  public String print(final ExpensesValidationReport expensesValidationReport)
  {
    return expensesValidationReport.getHeader() + "\n"
      + expensesLines(expensesValidationReport.getValidatedExpenseList());
  }

  private String expensesLines(final List<ValidatedExpense> validatedExpenseList)
  {
    return validatedExpenseList.stream().map(this::validatedExpenseForReport)
                        .collect(Collectors.joining("\n"));
  }

  private String validatedExpenseForReport(final ValidatedExpense validatedExpense)
  {
    String date = DateFormatUtils.format(validatedExpense.getDay(), "dd MMMM");
    return date + "\t" + validatedExpense.getCategory() + "\t" + validatedExpense.getAmount()
      + "\t" + validatedExpense.getStatus() + "\t" + validatedExpense.getAllowedAmount();
  }
}

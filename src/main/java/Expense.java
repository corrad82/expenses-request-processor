import java.math.BigDecimal;
import java.util.Date;

public class Expense
{
  private String id;
  private String category;
  private Date day;
  private BigDecimal amount;
  private boolean hasDocument;

  public String getId()
  {
    return id;
  }

  public void setId(final String id)
  {
    this.id = id;
  }

  public String getCategory()
  {
    return category;
  }

  public void setCategory(final String category)
  {
    this.category = category;
  }

  public BigDecimal getAmount()
  {
    return amount;
  }

  public void setAmount(final BigDecimal amount)
  {
    this.amount = amount;
  }

  public boolean isHasDocument()
  {
    return hasDocument;
  }

  public void setHasDocument(final boolean hasDocument)
  {
    this.hasDocument = hasDocument;
  }

  public Date getDay()
  {
    return day;
  }

  public void setDay(final Date day)
  {
    this.day = day;
  }
}

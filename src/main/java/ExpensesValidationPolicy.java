import java.util.Collections;
import java.util.List;

public class ExpensesValidationPolicy
{

  private final ValidationRule expenseWithDocumentValidationRule;
  private final ValidationRule amountValidationRule;
  private final ValidationRule foodAmountValidationRule;

  public ExpensesValidationPolicy(final ValidationRule expenseWithDocumentValidationRule,
    final ValidationRule amountValidationRule, final ValidationRule foodAmountValidationRule)
  {
    this.expenseWithDocumentValidationRule = expenseWithDocumentValidationRule;
    this.amountValidationRule = amountValidationRule;
    this.foodAmountValidationRule = foodAmountValidationRule;
  }

  public List<ValidatedExpense> apply(List<Expense> expenses)
  {
    return Collections.singletonList(validateExpense(expenses.get(0)));
  }

  private ValidatedExpense validateExpense(final Expense expense)
  {
    ValidatedExpense validatedExpense = expenseWithDocumentValidationRule.apply(expense);
    if (notValid(validatedExpense))
    {
      return validatedExpense;
    }

    validatedExpense = amountValidationRule.apply(expense);

    if (notValid(validatedExpense))
    {
      return validatedExpense;
    }

    return foodAmountValidationRule.apply(expense);
  }

  private boolean notValid(final ValidatedExpense validatedExpense)
  {
    return !ValidatedExpense.Status.OK.equals(validatedExpense.getStatus());
  }
}

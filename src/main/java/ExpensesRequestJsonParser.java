import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;

public class ExpensesRequestJsonParser
{
  public ExpensesRequest parse(final String json)
  {
    ObjectMapper mapper = new ObjectMapper();
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    mapper.setDateFormat(new ISO8601DateFormat());

    try
    {
      return mapper.readValue(json, ExpensesRequest.class);
    }
    catch (Exception e)
    {
      e.printStackTrace();
      return null;
    }
  }
}

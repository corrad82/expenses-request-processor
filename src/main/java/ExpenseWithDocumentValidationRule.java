import java.math.BigDecimal;


public class ExpenseWithDocumentValidationRule implements ValidationRule
{

	public ValidatedExpense apply(final Expense expense)
	  {
	    ValidatedExpense validatedExpense = ValidatedExpense.from(expense);

			if (!expense.isHasDocument())
			{
        validatedExpense.setStatus(ValidatedExpense.Status.NODOC);
        validatedExpense.setAllowedAmount(BigDecimal.ZERO);
        return validatedExpense;
      }
      return validatedExpense;
	  }
}

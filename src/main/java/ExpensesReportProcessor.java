import java.util.List;

public class ExpensesReportProcessor

{

  private final ExpensesRequestLoader expensesRequestLoader;
  private final ExpensesRequestValidator expensesRequestValidator;
  private final ReportGenerator reportGenerator;

  public ExpensesReportProcessor(final ExpensesRequestLoader expensesRequestLoader,
    final ExpensesRequestValidator expensesRequestValidator, final ReportGenerator reportGenerator)
  {
    this.expensesRequestLoader = expensesRequestLoader;

    this.expensesRequestValidator = expensesRequestValidator;
    this.reportGenerator = reportGenerator;
  }

  public String process(final String jsonPath)
  {

    ExpensesRequest expensesRequest = expensesRequestLoader.load(jsonPath);
    
    List<ValidatedExpense> validatedExpenseList =
      expensesRequestValidator.validate(expensesRequest.getExpenses());

    ExpensesValidationReport expensesValidationReport = new ExpensesValidationReport(validatedExpenseList);

    return reportGenerator.print(expensesValidationReport);
  }
}

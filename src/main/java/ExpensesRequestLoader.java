import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.stream.Collectors;



public class ExpensesRequestLoader {

	public ExpensesRequest load(String jsonPath) {
		
		BufferedReader bufferedReader = null;
		
	    try {
			 bufferedReader = new BufferedReader(new FileReader(jsonPath));
			
			return new ExpensesRequestJsonParser().parse(bufferedReader.lines().collect(Collectors.joining()));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				bufferedReader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		
		return null;
	}

}

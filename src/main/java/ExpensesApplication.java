public class ExpensesApplication
{

  public static void main(String[] args)
  {
    final ExpensesRequestLoader expensesRequestLoader = new ExpensesRequestLoader();

    final ExpensesValidationPolicy expensesValidationPolicy = new ExpensesValidationPolicy(
      new ExpenseWithDocumentValidationRule(),
      new AmountValidationRule(),
      new FoodAmountValidationRule());

    final ExpensesRequestValidator expensesRequestValidator = new ExpensesRequestValidator(
      expensesValidationPolicy);

    final ReportGenerator reportGenerator = new ReportGenerator();

    String processResult = new ExpensesReportProcessor(expensesRequestLoader, expensesRequestValidator, reportGenerator)
      .process("src/main/resources/request.json");

    System.out.println(processResult);
  }

}

import java.util.List;

public class ExpensesRequestValidator
{

  private final ExpensesValidationPolicy expensesValidationPolicy;

  public ExpensesRequestValidator(final ExpensesValidationPolicy expensesValidationPolicy)
  {
    this.expensesValidationPolicy = expensesValidationPolicy;
  }

  public List<ValidatedExpense> validate(final List<Expense> expenses)
  {
    return expensesValidationPolicy.apply(expenses);
  }
}

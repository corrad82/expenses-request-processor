import java.math.BigDecimal;

public class AmountValidationRule implements ValidationRule
{

  private static final BigDecimal ONE_HUNDRED = BigDecimal.valueOf(100L);

  @Override
  public ValidatedExpense apply(final Expense expense)
  {
    ValidatedExpense validatedExpense = ValidatedExpense.from(expense);
    if(expense.getAmount().compareTo(ONE_HUNDRED) > 0 )
    {
      validatedExpense.setStatus(ValidatedExpense.Status.PARTIAL);
      validatedExpense.setAllowedAmount(ONE_HUNDRED);
    }
    return validatedExpense;
  }
}

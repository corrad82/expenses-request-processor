import java.math.BigDecimal;

public class FoodAmountValidationRule implements ValidationRule
{

  private static final BigDecimal TWENTY_FIVE = BigDecimal.valueOf(25);

  @Override
  public ValidatedExpense apply(final Expense expense)
  {
    ValidatedExpense validatedExpense = ValidatedExpense.from(expense);

    if ("food".equals(expense.getCategory())
      && expense.getAmount().compareTo(TWENTY_FIVE) >= 0)
    {
      validatedExpense.setStatus(ValidatedExpense.Status.PARTIAL);
      validatedExpense.setAllowedAmount(TWENTY_FIVE);
    }
    return validatedExpense;
  }
}

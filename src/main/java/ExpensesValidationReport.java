import java.util.List;

public class ExpensesValidationReport
{
  private static final String HEADER = "Date\tCategory\tRequested Amount\tStatus\tAllowedAmount";

  private final List<ValidatedExpense> validatedExpenseList;

  public ExpensesValidationReport(final List<ValidatedExpense> validatedExpenseList)
  {
    this.validatedExpenseList = validatedExpenseList;
  }

  public List<ValidatedExpense> getValidatedExpenseList()
  {
    return validatedExpenseList;
  }

  public String getHeader()
  {
    return HEADER;
  }
}

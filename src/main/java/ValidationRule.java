public interface ValidationRule
{
   ValidatedExpense apply(final Expense expense);
}
